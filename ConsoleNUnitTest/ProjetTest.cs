using ConsoleNUnit;
using NUnit.Framework;
using System;

namespace Tests
{
    public class ProjetTest
    {
        private readonly TimeSpan _2h_    = TimeSpan.FromHours(2);
        private readonly TimeSpan _5min_  = TimeSpan.FromMinutes(5);
        private readonly TimeSpan _10min_ = TimeSpan.FromMinutes(10);
        private readonly TimeSpan _15min_ = TimeSpan.FromMinutes(15);
        private readonly TimeSpan _20min_ = TimeSpan.FromMinutes(20);
        private readonly TimeSpan _50min_ = TimeSpan.FromMinutes(50);
        private readonly TimeSpan _1h45_  = new TimeSpan(hours: 1, minutes: 45, seconds: 0);
        private readonly TimeSpan _1h40_  = new TimeSpan(hours: 1, minutes: 40, seconds: 0);

        [SetUp]
        public void Init()
        {
            // Code ex�cut� avant chaque cas de test
            // Non utilis�
        }

        [Test]
        public void InitialisationDUnProjetBasique()
        {
            // Arrange
            var nomDeProjet = "Un projet";
            var dureeEstimee = _2h_;

            // Act
            var test = new Projet(nomDeProjet, dureeEstimee);

            // Assert
            Assert.AreEqual("Un projet"         , test.Nom);
            Assert.AreEqual(EtatProjet.EnAvance , test.Etat);
            Assert.AreEqual(TimeSpan.Zero       , test.TempsEffectif);
            Assert.AreEqual(_2h_                , test.DureeEstimee);
            Assert.AreEqual(_2h_                , test.TempsRestant);
        }

        [Test]
        public void InitialisationDUnProjetSansNom()
        {
            // Arrange & Act
            var test = new Projet(string.Empty, _2h_);

            // Assert
            Assert.AreEqual(string.Empty, test.Nom);
            Assert.AreEqual(EtatProjet.EnAvance, test.Etat);
            Assert.AreEqual(TimeSpan.Zero, test.TempsEffectif);
            Assert.AreEqual(_2h_, test.DureeEstimee);
            Assert.AreEqual(_2h_, test.TempsRestant);
        }


        [Test]
        public void InitialisationDUnProjetAvecNomNull()
        {
            // D�conseill�
            // try
            // {
            //     var test = new Projet(null, _2h_);
            //     Assert.Fail();
            // }
            // catch(NullReferenceException e)
            // {
            //     Assert.Pass();
            // }
            Assert.Throws<NullReferenceException>(() => {
                var test = new Projet(null, _2h_);
            });
        }

        [Test]
        public void InitialisationDUnProjetDeDuree0()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => {
                var test = new Projet("Un projet", TimeSpan.Zero);
            });
        }
        [Test]
        public void InitialisationDUnProjetDeDureeNegative()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => {
                var test = new Projet("Un projet", -_2h_);
            });
        }
        [Test]
        public void InitialisationDUnProjetMargeNegative()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => {
                new Projet("Un projet", _2h_, -_5min_);
            });
        }
        [Test]
        public void InitialisationDUnProjetAvecMargeExcedant20pourcent()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => {
                new Projet("Un projet", _50min_, _15min_);
            });
        }
        [Test]
        public void InitialisationDUnProjetAvecMargeEgaleA20pourcent()
        {
            var test = new Projet("Un projet", _50min_, _10min_);
            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.EnAvance,
                tempsEffectifAttendu : TimeSpan.Zero,
                tempsRestantAttendu  : _50min_, projetReel: test
            );
        }
        private Projet CreerUnProjet2h() => new Projet("Un projet", _2h_);
        private Projet CreerUnProjet2hMarge5min() => new Projet("Un projet", _2h_, _5min_);

        private void AssertTempsDeProjet(
            EtatProjet etatAttendu,         //expected
            TimeSpan tempsEffectifAttendu,  //expected
            TimeSpan tempsRestantAttendu,   //expected
            Projet projetReel // actual
        )
        {
            Assert.AreEqual(etatAttendu         , projetReel.Etat);
            Assert.AreEqual(tempsEffectifAttendu, projetReel.TempsEffectif);
            Assert.AreEqual(tempsRestantAttendu , projetReel.TempsRestant);
        }

        [Test]
        public void ComptabilisationDUneTacheCourteDansLeTempsEstime()
        {
            var test = CreerUnProjet2h();

            test.ComptabiliserTache(_15min_);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.EnAvance,
                tempsEffectifAttendu : _15min_            ,
                tempsRestantAttendu  : _1h45_             , projetReel: test
            );
            Assert.AreEqual(_2h_   , test.DureeEstimee);
        }

        [Test]
        public void ComptabilisationDe2TachesCourtesDansLeTempsEstime()
        {
            var test = CreerUnProjet2h();

            test.ComptabiliserTache(_5min_);
            test.ComptabiliserTache(_15min_);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.EnAvance,
                tempsEffectifAttendu : _20min_,
                tempsRestantAttendu  : _1h40_ , projetReel: test
            );
        }

        [Test]
        public void ComptabilisationDUneTacheInstantanee()
        {
            var test = CreerUnProjet2h();

            test.ComptabiliserTache(TimeSpan.Zero);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.EnAvance,
                tempsEffectifAttendu : TimeSpan.Zero,
                tempsRestantAttendu  : _2h_ , projetReel: test
            );
        }

        [Test]
        public void ComptabilisationDUneTacheLongueAuDelaDuProjet()
        {
            var test = new Projet("Un projet", _1h40_);

            test.ComptabiliserTache(_2h_);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.EnRetard,
                tempsEffectifAttendu : _2h_,
                tempsRestantAttendu  : - _20min_, projetReel: test
            );
        }
    
        [Test]
        public void ComptabilisationDe2TachesAvecDepassementDeLaSeconde()
        {
            var test = CreerUnProjet2h();

            test.ComptabiliserTache(_5min_);
            test.ComptabiliserTache(_2h_);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.EnRetard,
                tempsEffectifAttendu : new TimeSpan(hours:2, minutes:5, seconds:0),
                tempsRestantAttendu  : - _5min_,  projetReel: test
            );
        }
        [Test]
        public void ComptabilisationDUneTacheEgaleALaDureeEstimee()
        {
            var test = CreerUnProjet2h();

            test.ComptabiliserTache(_2h_);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.DansLesTemps,
                tempsEffectifAttendu : _2h_,
                tempsRestantAttendu  : TimeSpan.Zero, projetReel: test
            );
        }
        [Test]
        public void ComptabilisationDe2TachesCorrespondantALaDureeEstimee()
        {
            var test = CreerUnProjet2h();

            test.ComptabiliserTache(_15min_);
            test.ComptabiliserTache(_1h45_);

            AssertTempsDeProjet(
                etatAttendu          : EtatProjet.DansLesTemps,
                tempsEffectifAttendu : _2h_,
                tempsRestantAttendu  : TimeSpan.Zero, projetReel: test
            );
        }
        [Test]
        public void ComptabilisationDUneTacheDeDureeNegative()
        {
            var test = CreerUnProjet2h();

            Assert.Throws<ArgumentOutOfRangeException>(() => {
                test.ComptabiliserTache(- _5min_);
            });
        }
        // ---------------------:------2h------:-------
        //                                 ^
        //                    1h55      [2h03] 2h05
        [Test]
        public void ComptabilisationDUneTacheDepassantLaDureeEstimeeComprisDansLaMarge()
        {
            var test = CreerUnProjet2hMarge5min();

            test.ComptabiliserTache(new TimeSpan(hours: 2, minutes: 3, seconds: 0));

            Assert.AreEqual(EtatProjet.DansLesTemps, test.Etat);
        }

        // ---------------------:------2h------:-------
        //                                     ^
        //                    1h55           [2h05]
        [Test]
        public void ComptabilisationDe2TachesAvecDepassementDeLaDureeEstimeeEgalALaMarge()
        {
            var test = CreerUnProjet2hMarge5min();

            test.ComptabiliserTache(_2h_);
            test.ComptabiliserTache(_5min_);

            Assert.AreEqual(EtatProjet.DansLesTemps, test.Etat);
        }

        // ---------------------:------2h------:-------
        //                           ^
        //                    1h55 [1h58]     2h05
        [Test]
        public void ComptabilisationDe2TachesAvantDureeEstimeeEtComprisesDansLaMarge()
        {
            var test = CreerUnProjet2hMarge5min();

            test.ComptabiliserTache(_1h40_);
            test.ComptabiliserTache(TimeSpan.FromMinutes(18));
            Assert.AreEqual(EtatProjet.DansLesTemps, test.Etat);
        }
        // ---------------------:------2h------:-------
        //                      ^
        //                    [1h55]          2h05
        [Test]
        public void ComptabilisationDUneTacheAvantDureeEstimeeEtEgaleALaMarge()
        {
            var test = CreerUnProjet2hMarge5min();
            test.ComptabiliserTache(_1h40_);
            test.ComptabiliserTache(_15min_);
            Assert.AreEqual(EtatProjet.DansLesTemps, test.Etat);
        }

    }
}