﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleNUnit
{
    public enum EtatProjet
    {
        EnAvance,
        DansLesTemps,
        EnRetard
    }
    public class Projet
    {
        private TimeSpan _cumulTaches = new TimeSpan();
        private TimeSpan _marge;

        /// <summary>
        /// Initialise un projet avec mesure effective du temps de travail.
        /// </summary>
        /// <param name="nom">Nom du projet</param>
        /// <param name="duree">Durée estimée du projet</param>
        /// <param name="marge">
        ///     Marge dans laquelle le projet est 
        ///     considéré "dans les temps"
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Levée si la duree est négative ou nulle ou si la
        /// marge excède 20% de la duree ou est négative
        /// </exception>
        /// <remarks>
        /// La marge sert à déterminer l'état du projet. Le projet est considéré 
        /// "dans les temps" si le temps effectif est proche du temps estimé plus
        /// ou moins la marge
        /// </remarks>
        public Projet(string nom, TimeSpan duree, TimeSpan marge)
        {
            if(nom==null)
            {
                throw new NullReferenceException("Nom ne peut être null");
            }
            if(duree <= TimeSpan.Zero)
            {
                throw new ArgumentOutOfRangeException("La durée du projet doit être strictement positive");
            }
            if(marge < TimeSpan.Zero || marge > duree * 0.2)
            {
                throw new ArgumentOutOfRangeException("La durée de la marge ne peut être négative ou excéder 20% du temps du projet");
            }
            Nom = nom;
            DureeEstimee = duree;
            _marge = marge;
        }
        public Projet(string nom, TimeSpan duree) : this(nom, duree, TimeSpan.Zero)
        {
            
        }
        public string Nom { get; private set; }
        public TimeSpan DureeEstimee { get; private set; }

        public void ComptabiliserTache(TimeSpan duree)
        {
            if(duree < TimeSpan.Zero)
            {
                throw new ArgumentOutOfRangeException("La durée de la tâche doit être positive");
            }
            _cumulTaches += duree;
        }
        public TimeSpan TempsEffectif => _cumulTaches;

        public EtatProjet Etat
        {
            get
            {
                if(_cumulTaches < DureeEstimee - _marge)
                {
                    return EtatProjet.EnAvance;
                }
                else if(_cumulTaches > DureeEstimee + _marge)
                {
                    return EtatProjet.EnRetard;
                }
                else
                {
                    return EtatProjet.DansLesTemps;
                }
            }
        }

        public TimeSpan TempsRestant => DureeEstimee - TempsEffectif;
    }
}
