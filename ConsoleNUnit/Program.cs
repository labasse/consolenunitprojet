﻿using System;

namespace ConsoleNUnit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nom du projet : ");
            var nom = Console.ReadLine();
            int duree;
            string saisie;

            do
            {
                Console.Write("Durée estimée du projet en heure (1-72) : ");
                saisie = Console.ReadLine();
            }
            while (!int.TryParse(saisie, out duree) || duree<1 || duree>72);

            var monProjet = new Projet(nom, TimeSpan.FromHours(duree));
            var action = "Commencer";
            var fini = false;
            DateTime? debutTache = null;

            while(!fini)
            {
                Console.WriteLine( "1 - Consulter l'état du projet" );
                Console.WriteLine($"2 - {action} une tâche" );
                Console.WriteLine( "3 - Quitter" );
                Console.Write("Votre choix ? ");
                var choix = Console.ReadKey();

                Console.WriteLine();
                switch (choix.KeyChar)
                {
                    case '1':
                        Console.WriteLine( "Projet en cours :");
                        Console.WriteLine($"- Nom du projet  : {monProjet.Nom}");
                        Console.WriteLine($"- Durée estimée  : {monProjet.DureeEstimee}");
                        Console.WriteLine($"- Temps passé    : {monProjet.TempsEffectif:hh':'mm':'ss}");
                        Console.WriteLine($"- Temps restant  : {monProjet.TempsRestant:hh':'mm':'ss}");
                        Console.WriteLine($"- Etat du projet : {monProjet.Etat}");
                        break;
                    case '2':
                        if(debutTache == null)
                        {
                            // L'utilisateur démarre une tâche
                            debutTache = DateTime.Now;
                            action = "Terminer";
                            Console.WriteLine($"Tâche démarrée le {debutTache:g}");
                        }
                        else
                        {
                            // L'utilisateur termine la tâche
                            var dureeTache = DateTime.Now - debutTache.Value;

                            Console.WriteLine($"Tâche terminée le {DateTime.Now:g}, durée : {dureeTache:hh':'mm':'ss}");
                            monProjet.ComptabiliserTache(dureeTache);
                            debutTache = null;
                            action = "Commencer";
                        }
                        break;
                    case '3':
                        fini = true;
                        break;
                    default:
                        Console.Error.WriteLine("Choix non valide");
                        break;
                }
            }
            Console.WriteLine("Au revoir !");
        }
    }
}
